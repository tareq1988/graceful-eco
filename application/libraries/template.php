<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Template
{
    protected $CI;
    protected $header_js;
    protected $header_css;
    protected $footer_js;

    protected $header_tpl;
    protected $footer_tpl;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->header_js = array(
            'http://maps.googleapis.com/maps/api/js?sensor=false',
            '//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js'
        );
        $this->footer_js = array(
            '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js',
            base_url().'asset/js/jquery.locationpicker.js',
            base_url().'asset/js/jquery.gomap-1.3.2.min.js',
            base_url().'asset/js/bootstrap.min.js',
            base_url().'asset/js/jquery.validate.min.js',
            '//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.min.js',
            base_url().'asset/js/scripts.js'
        );
        $this->header_css = array(
            base_url().'asset/css/bootstrap.css',
            base_url().'asset/css/style.css'
        );
        $this->header_tpl = 'header';
        $this->footer_tpl = 'footer';
    }

    public function add_js($url, $place = 'header')
    {
        $url = trim($url);
        if($place == 'header')
        {
            $this->header_js[] = $url;
        }
        if($place = 'footer')
        {
            $this->footer_js[] = $url;
        }
    }

    public function add_css($url)
    {
        $this->header_css[] = trim($url);
    }

    public function change_header($tpl)
    {
        $this->header_tpl = trim($tpl);
    }

    public function change_footer($tpl)
    {
        $this->footer_tpl = trim($tpl);
    }

    function load($view = '', $data = '')
    {
        $data['header_js'] = $this->header_js;
        $data['footer_js'] = $this->footer_js;
        $data['header_css'] = $this->header_css;
        $this->CI->load->view($this->header_tpl, $data);
        $this->CI->load->view($view, $data);
        $this->CI->load->view($this->footer_tpl, $data);
    }
}