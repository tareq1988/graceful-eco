<?php
class Auth
{
    public $CI;

    function __construct()
    {
        $this->CI =& get_instance();
    }

    function is_logged_in()
    {
        $log = $this->CI->session->userdata('logged_in');

        return $log;
    }

    function get_role() {
        $type = $this->CI->session->userdata('type');

        return $type;
    }
}