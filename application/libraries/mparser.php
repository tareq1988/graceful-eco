<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mparser {
	public $address;
	public $longitude;
	public $latitude;
	public $message;
	public $errors = array();


	public function init($msg) {
		$this->message = $msg;
		$this->_prepare_msg();
		$this->_get_address();
	}

	private function _prepare_msg() {
		if (strlen($this->message) > 160) {
			$this->errors[] = "big_msg";
			return;
		}
		$parts = explode(".", $this->message);
		if (count($parts) != 2) {
			$this->errors[] = "no_address";
			return;
		}
		$this->address = trim($parts[0], ",");
		$this->message = ltrim($parts[1], ",.");
	}

	private function _get_address() {
		$found = false;
		$jsondata = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=" . urlencode($this->address) . "&sensor=false");
		$outputs = json_decode($jsondata, true);

		if ($outputs['status'] == "ZERO_RESULTS") {
			$this->errors[] = "not_found";
			return;
		}
		foreach ($outputs['results'] as $k1 => $v1) {
			foreach ($v1['address_components'] as $v2) {
				if ($v2['short_name'] == "BD") {
					$found = true;
					$this->latitude  = $v1['geometry']['location']['lat'];
					$this->longitude = $v1['geometry']['location']['lng'];
					break;
				}
			}
			if ($found) {
				break;
			}
		}
		if (!$found) {
			$this->errors[] = "not_found";
		}
	}
}

?>