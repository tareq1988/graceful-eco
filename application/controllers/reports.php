<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('report');
        $this->load->helper('date');
        $this->load->helper('url');

        if( !$this->auth->is_logged_in() ) {
            redirect('/account/login');
        }
    }

    public function index()
    {
        $data['reports'] = $this->report->get_all();
        $data['menu'] = 'report_list';

        $this->template->load('reportlist', $data);
    }

    public function details($id)
    {
        $data['details'] = $this->report->get($id);
        $this->template->load('report_detail', $data);

    }

    public function deny()
    {

    }

    public function approve($id)
    {
	    $this->db->update("reports", array('verified' => 1),array('id' => $id), 1);
	    redirect($_SERVER['HTTP_REFERER']);
    }

    public function search()
    {

    }
}
