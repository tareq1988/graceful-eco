<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('rb');
	}

        /**
         * SMS Gatway callback. It gets incoming sms on this url and echos an 
         * empty response so that twilio stop processing any farther.
         * Response is generated using syntax found on 
         * http://www.twilio.com/docs/api/twiml/sms/your_response
         */
	public function sms() {
		$_POST['Body'] = "FFF.Eshan";
		$_POST['From'] = "123";
		$_POST['SmsSid'] = "abc";
		$_POST['AccountSid'] = "AC12345678901234567890123456789012";
		$this->load->library('form_validation');
		$this->form_validation->set_rules("Body", "Body", "required");
		$this->form_validation->set_rules("From", "Form", "required");
		$this->form_validation->set_rules("SmsSid", "SmsSid", "required");
		$this->form_validation->set_rules("AccountSid", "AccountSid", "required|callback_ssid_check");
		if ($this->form_validation->run()) {
			$this->load->library('mparser');
			$this->mparser->init($this->input->post('Body'));
			if (count($this->mparser->errors) < 1) {
				$report = R::dispense('reports');
				$report->source_name = $this->input->post('From');
				$report->source_type = "SMS";
				$report->source      = $this->input->post("From");
				$report->location    = $this->mparser->address;
				$report->latitude    = $this->mparser->latitude;
				$report->longitude   = $this->mparser->longitude;
				$report->description = $this->mparser->message;
				$id = R::store($report);
			}
		} else {
			echo 'error';
		}
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Response></Response>";
	}

	public function ssid_check($str) {
		if ($str == $this->config->item('twilio_appsid')) {
			return true;
		}

		return false;
	}

	public function save() {
		// read all the post data
		// sanitize post data
		// save on database
		// echo an empty response in JSON
		if ($this->input->post("api_key") == $this->config->item("api_key")) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules("name", "Name", "trim|required");
			$this->form_validation->set_rules("email", "Email", "trim|valid_email|required");
			$this->form_validation->set_rules("address", "Address", "trim|required");
			$this->form_validation->set_rules("latlang", "Latitude", "trim|required");
			$this->form_validation->set_rules("details", "details", "trim|required");
			$this->form_validation->set_rules("plants", "Plants", "trim");

			if ($this->form_validation->run() !== FALSE) {
				$report = R::dispense('reports');
				$report->location = $this->input->post("address");
				$latlong = explode(",", $this->input->post("latlang"));
				$report->latitude = $latlong[0];
				$report->longitude = $latlong[1];
				$report->source_name = $this->input->post("name");
				$report->source = $this->input->post("email");
				$report->source_type = "API";
				$report->description = $this->input->post("details");

				if ($this->input->post("plants")) {
					$plants = explode(",", $this->input->post("plants"));
					if (count ($plants) > 0) {
						foreach ($plants as $p) {
							if (!R::findOne("plants", " name = ?", array($p))) {
								$plant = R::dispense("plants");
								$plant->name = $p;
							}
						}
					}
				}

				if (!empty($_FILES)) {
					$this->load->library("upload");
					$upload_config['encrypt_name']  = true;
					if (!empty($_FILES['image']['tmp_name']) && file_exists($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
						$upload_config['upload_path']   = "./uploads/images/";
						$upload_config['allowed_types'] = "git|jpg|png";

						$this->upload->initialize($upload_config);
						if ($this->upload->do_upload('image')) {
							$upload_data = $this->upload->data();
							$report->image = $upload_data['file_name'];
						}
					}

					if (!empty($_FILES['video']['tmp_name']) && file_exists($_FILES['video']['tmp_name']) && is_uploaded_file($_FILES['video']['tmp_name'])) {
						$upload_config['upload_path']   = "./uploads/videos/";
						$upload_config['allowed_types'] = "mp4|3gp";

						$this->upload->initialize($upload_config);
						if ($this->upload->do_upload('video')) {
							$upload_data = $this->upload->data();
							$report->video = $upload_data['file_name'];
						}
					}
				}


				$tech_name = $this->input->post("tech_name");
				$tech_value = $this->input->post("tech_value");
				if ($tech_name && $tech_value) {
					$c = count($tech_name);
					for ($i = 0; $i < $c; $i++) {
						if (!empty ($tech_name[$i]) && !empty ($tech_value[$i])) {
							$technical = R::dispense("technicals");
							$technical->name  = $tech_name[$i];
							$technical->value = $tech_value[$i];
							$report->ownTechnicals[] = $technical;
						}
					}
				}
				R::store($report);
				echo json_encode(array('status' => "successful"));
			}
			else {
				echo json_encode(array('status' => "error"));
			}
		}
		else {
			echo json_encode(array('status' => "error"));
		}
	}
}