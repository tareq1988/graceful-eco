<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of account
 *
 * @author tareq
 */
class Account extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('authorize');
    }

    /**
     * Account index
     *
     * @auther  Tareq
     * @access  public
     */
    function index()
    {
        $this->login();
    }

    /**
     * do login
     *
     */
    function login()
    {
        //if the user is not logged in, we show him the login form
        if (!$this->auth->is_logged_in()) {
            $form_submitted = $this->input->post('submit');
            if (isset($form_submitted)) //if form is submitted
            {
                $this->load->library('form_validation');

                $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|min_length[2]|max_length[20]|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|xss_clean');

                //user failed in form_validation
                if ($this->form_validation->run() == FALSE) {
                    $this->login_form();
                } //form validation is ok, now check username, password
                else {
                    $check = $this->check_un_pwd();

                    //invalid username/pass
                    if (!$check) {
                        $this->session->set_flashdata('invalid_login', 'Invalid E-Mail/Password');
                        $data['invalid_login'] = 'Invalid E-Mail/Password';
                        //$this->login_form($data);
                        redirect('account/login_form');
                    } //username/pass is right, now is his account activated yet?
                    else {
                        //if account is inactive
                        $active = $this->is_account_active();
                        if (!$active) {
                            redirect('account/inactive');
                        } //ok, he is all ok, let him log in
                        else {
                            $this->_logged_in();
                            redirect('/reports');
                        }
                    }
                }
            } //user has not submitted the form yet, show him the form
            else {
                $this->login_form();
            }
        } //he is already logged in
        else {
            $data['page_title'] = 'Already logged in';
            $this->template->load('login/already_logged', $data);
        }
    }

    /**
     * Show login form
     *
     * @param <array> $data page properties
     */
    function login_form($data = '')
    {
        //$this->load->view('login/login_form');
        $data['page_title'] = 'Login';
        $this->template->load('login/login_form', $data);
    }

    /**
     * logout user
     */
    function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('type');
        $this->session->set_flashdata('logged_out', 'You are successfully logged out');
        redirect('account/login', '');
        $this->login();
    }

    /**
     * register user account
     */
    function register()
    {
        if (!$this->auth->is_logged_in()) {
            $submitted = $this->input->post('reg_submit');

            if ($submitted) {
                unset($submitted);

                $this->load->library('form_validation');

                $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[2]|max_length[20]|xss_clean');
                $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[2]|xss_clean');
                $this->form_validation->set_rules('c_password', 'Confirm Password', 'trim|required|min_length[2]|xss_clean|matches[password]');

                if ($this->form_validation->run() == FALSE) {
                    $this->reg_form();
                } else {
                    if ($this->_is_email_exists()) {
                        $this->session->set_flashdata('email_exists', 'This E-Mail id is already registered, please choose another');
                        //$this->reg_form();
                        redirect('account/reg_form');
                    } else {
                        $new_user = $this->authorize->new_member();

                        if ($new_user) {
                            $this->template->load('reg/reg_success');
                        } else show_error('Something went wrong');
                    }
                }
            } else {
                $this->reg_form();
            }
        } else {
            $this->template->load('login/already_logged');
        }
    }

    /**
     * Show registration form
     */
    function reg_form()
    {
        $data['page_title'] = "Signup";
        $this->template->load('reg/reg_form', $data);
    }

    /**
     * Show forgot password page
     */
    function forgot_password()
    {
        $data['page_title'] = "Forgot Password";
        $this->template->load('account_reset', $data);
    }

    /**
     * Change password
     */
    function change_password()
    {

    }

    /**
     * activate user account
     *
     * @param <string> $code activation code
     */
    function activate($code = '')
    {
        $activate = $this->authorize->activate($code);
        if ($activate) {
            $this->session->set_flashdata('activated', 'Your account is now active. Please login');
            redirect('account/login', 'refresh');
        } else {
            redirect('account/login', 'refresh');
        }
    }

    /**
     * Set the session data for logged in user
     */
    function _logged_in()
    {
        $this->load->model('user_model');
        $email = $this->input->post('email');
        $user = $this->user_model->get_user_by_email($email);

        $this->session->set_userdata('logged_in', true);
        $this->session->set_userdata('name', $user->name);
        $this->session->set_userdata('email', $user->email);
        $this->session->set_userdata('user_id', $user->id);
        $this->session->set_userdata('type', $user->type);

        //update last login on db
        $this->user_model->update_last_login($user->id);
    }


    /**
     * Check username/password
     *
     * @return <bool>
     */
    function check_un_pwd()
    {
        $status = $this->authorize->check_un_pwd();

        if ($status) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check if account is active
     *
     * @return bool
     */
    function is_account_active()
    {
        $status = $this->authorize->is_account_active();

        if ($status) {
            return true;
        } else {
            return false;
        }
    } //end is_account_active

    /**
     * check if email already registered
     *
     * @return bool
     */
    function _is_email_exists()
    {
        $email = $this->input->post('email');
        if ($this->authorize->email_exists($email)) {
            return true;
        }
    }

    /**
     * Show the inactive view page
     */
    function inactive()
    {
        $data['page_title'] = "Inactive account";
        $this->template->load('login/account_inactive', $data);
    }

    /*
     * Re-Send activation code
     */
    function resend_activation()
    {
        $status = $this->authorize->email_exists();
        $active = $this->is_account_active();

        if ($status && !$active) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|min_length[2]|max_length[20]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                redirect('account/inactive', 'refresh');
            }
            {
                $code = $this->authorize->resend_activation();
                $this->session->set_flashdata('activation_resend', 'Your new activation mail has been sent. Please check your mail inbox: ' . $code);
                redirect('account/inactive', 'refresh');
            }
        } else {
            $this->session->set_flashdata('invalid_email', 'Invalid E-Mail/Account is already activated');
            redirect('account/inactive', 'refresh');
        }
    }

    /**
     * Reset user password
     */
    function reset()
    {
        echo 'Reset Account';
    } // End reset
}