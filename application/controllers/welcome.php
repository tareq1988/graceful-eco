<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

	    $this->load->library('rb');
    }

    public function index()
    {
        $this->load->model('report');

        $data['reports'] = $this->report->get_all();
        $this->template->load('home', $data);
    }
    public function repolist()
    {
        $this->template->load('reportlist');
    }

    function xreport()
    {
        $this->load->helper('form');

        if ($this->input->post()) {
            $name = $this->input->post('name');
            $address = $this->input->post('address');
            $details = $this->input->post('details');
            $plants = $this->input->post('plants');
            $ph = $this->input->post('ph');
            $chromium = $this->input->post('chromium');

            var_dump( $_POST );
        }

        $this->template->load('add_report');
    }

	public function report() {
        $data['menu'] = 'new_report';

		$this->load->library('form_validation');
		$this->form_validation->set_rules("name", "Name", "required");
		$this->form_validation->set_rules("email", "Email", "valid_email|required");
		$this->form_validation->set_rules("address", "Address", "required");
		$this->form_validation->set_rules("latlang", "Latitude", "required");
		$this->form_validation->set_rules("details", "details", "required");
		//$this->form_validation->set_rules("plants", "Plants", "required");

		$data['successful'] = false;
		if ($this->form_validation->run() !== FALSE) {
			$data['successful'] = true;
			$report = R::dispense('reports');
			//$report.php->name = $this->input->post("name");
			$report->location = $this->input->post("address");
			$latlong = explode(",", $this->input->post("latlang"));
			$report->latitude = $latlong[0];
			$report->longitude = $latlong[1];
			//$report.php->email = $this->input->post("email");
			$report->description = $this->input->post("details");
			$report->source_name = $this->input->post("name");
			$report->source = $this->input->post("email");
			$report->source_type = "WEB";


			if (!empty($_FILES)) {
				$this->load->library("upload");
				$upload_config['encrypt_name']  = true;
				if (!empty($_FILES['image']['tmp_name']) && file_exists($_FILES['image']['tmp_name']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
					$upload_config['upload_path']   = "./uploads/images/";
					$upload_config['allowed_types'] = "git|jpg|png";

					$this->upload->initialize($upload_config);
					if ($this->upload->do_upload('image')) {
						$upload_data = $this->upload->data();
						$report->image = $upload_data['file_name'];
					}
				}

				if (!empty($_FILES['video']['tmp_name']) && file_exists($_FILES['video']['tmp_name']) && is_uploaded_file($_FILES['video']['tmp_name'])) {
					$upload_config['upload_path']   = "./uploads/videos/";
					$upload_config['allowed_types'] = "mp4|3gp";

					$this->upload->initialize($upload_config);
					if ($this->upload->do_upload('video')) {
						$upload_data = $this->upload->data();
						$report->video = $upload_data['file_name'];
					}
				}
			}

			$tech_name = $this->input->post("tech_name");
			$tech_value = $this->input->post("tech_value");
			if ($tech_name && $tech_value) {
				$c = count($tech_name);
				for ($i = 0; $i < $c; $i++) {
					if (!empty ($tech_name[$i]) && !empty ($tech_value[$i])) {
						$technical = R::dispense("technicals");
						$technical->name  = $tech_name[$i];
						$technical->value = $tech_value[$i];
						$report->ownTechnicals[] = $technical;
					}
				}
			}
			R::store($report);

            unset( $_POST );
		}
		$this->template->load('add_report', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */