<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class En extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->template->change_header('header_landing');
        $this->template->change_footer('footer_landing');
        $this->template->add_js(base_url().'asset/js/linkify.js');
        $this->template->add_js('//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js');
        $this->template->add_js(base_url().'asset/js/jquery.blockUI.js');
        $this->template->add_js(base_url().'asset/js/jquery.qtip.min.js');
        $this->template->add_js('//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.1/jquery.fancybox.pack.js');
        $this->template->add_js(base_url().'asset/js/jquery.supersized.min.js');
        $this->template->add_js(base_url().'asset/js/jquery.supersized.shutter.min.js');
        $this->template->add_js(base_url().'asset/js/jquery.elastic.source.js');
        $this->template->add_js(base_url().'asset/js/jquery.infieldlabel.min.js');
        $this->template->add_js(base_url().'asset/js/jquery.carouFredSel.packed.js');
        $this->template->add_js(base_url().'asset/js/main.js');
        $this->template->add_js(base_url().'asset/js/contact-form.js');
        $this->template->add_js(base_url().'asset/js/newsletter-form.js');

        $this->template->add_css(base_url().'asset/css/jquery.qtip.css');
        $this->template->add_css(base_url().'asset/css/jquery-ui.css');
        $this->template->add_css(base_url().'asset/css/supersized.css');
        $this->template->add_css(base_url().'asset/css/supersized.shutter.css');
        $this->template->add_css(base_url().'asset/css/jquery.fancybox.css');
        $this->template->add_css(base_url().'asset/css/base.css');
        //$this->template->add_css(base_url().'asset/css/width-0-969.css');
        //$this->template->add_css(base_url().'asset/css/width-0-767.css');
        //$this->template->add_css(base_url().'asset/css/width-480-969.css');
        //$this->template->add_css(base_url().'asset/css/width-768-969.css');
        //$this->template->add_css(base_url().'asset/css/width-480-767.css');
        //$this->template->add_css(base_url().'asset/css/width-0-479.css');
        $this->template->add_css('http://fonts.googleapis.com/css?family=Voces');
        $this->template->add_css('http://fonts.googleapis.com/css?family=Dosis:400,300,200,500,600,700,800');

        $this->template->change_header('header_landing');
        $this->template->change_footer('footer_landing');
    }

    public function index()
    {
        $this->load->model('report');

        $data['reports'] = $this->report->get_all();
        $this->template->load('home_landing', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/en.php */