<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class User_model extends CI_Model
{
    /**
     * Get user data by his name
     * 
     * @param <string> $username
     * @return <int> user id
     */
	function get_user_by_name($username)
	{
		$this->db->select('*');
		$this->db->where('username', $username);
		$query = $this->db->get('users', 1);
		//return $query;

		if($query->num_rows > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data[0];
		}
	} //end get_user_by_name

    /**
     * Get user data by his email id
     * @param <sting> $email
     * @return <string> email id
     */
	function get_user_by_email($email)
	{
		$this->db->select('*');
		$this->db->where('email', $email);
		$query = $this->db->get('users', 1);
		//return $query;

		if($query->num_rows > 0)
		{
			foreach ($query->result() as $row)
			{
				$data[] = $row;
			}
			return $data[0];
		}
	} //end get_user_by_email


    /**
     * Edit user profile
     */
	function edit_profile ()
	{
        $fname  = $this->input->post('fname');
        $lname  = $this->input->post('lname');
        $id     = $this->input->post('id');
        $url    = $this->input->post('url');
        $yahoo  = $this->input->post('yahoo');
        $gtalk  = $this->input->post('gtalk');

        $data = array(
            'fname' => $fname,
            'lname' => $lname,
            'url'   => $url,
            'yahoo' => $yahoo,
            'gtalk' => $gtalk
        );

        $this->db->where('id', $id);
        $this->db->update('users', $data);
        
	} // End edit_profile

    /**
     * update last login datetime
     * 
     * @param <int> $id user_id
     */
    function update_last_login($id)
    {
        $data = array(
            'last_login' => date("Y-m-d H:i:s")
        );
        
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }
}
?>
