<?php
class Authorize extends CI_Model
{
    /*
     * Check username and password
     *
     * @param   string
     * @param   string
     * @return  boolean
     */
    function check_un_pwd()
    {
        $email = $this->input->post('email');
        $pwd = md5($this->input->post('password'));

        $this->db->select('id, name');
        $this->db->where('email', $email);
        $this->db->where('password', $pwd );

        $query = $this->db->get('users');

        if($query->num_rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function is_account_active()
    {
        $email = $this->input->post('email');
        $this->db->select('id, name');
        $this->db->where('email', $email);
        $this->db->where('active', '1');
        $query = $this->db->get('users');

        if($query->num_rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function new_member()
    {
        $this->load->helper('string');
        
        $new_member_insert_data = array(
            'name'  =>  $this->input->post('name'),
            'email'     =>  $this->input->post('email'),
            'password'  =>  md5($this->input->post('password')),
            'registered'=>  date('Y-m-d H:i:s'),
            'activation_code'   => random_string('unique', 16),
            'type' => $this->input->post('type'),
            'active' => 1
        );

        $insert = $this->db->insert('users', $new_member_insert_data);
        return $insert;
    }

    function email_exists($email)
    {

        $query = $this->db->get_where('users', array('email' => $email), 1);

        if($query->num_rows > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	
	function resend_activation($email)
	{
		$data = array(
			'activation_code'	=> random_string('unique', 16)
		);

		$this->db->where('email', $email);
		$update = $this->db->update('users', $data);

		return $update;
	}

	function activate($code = '')
	{
		$this->db->set('active', '1');
		$this->db->set('activation_code', 'activated');
		$this->db->where('activation_code', $code);
		$this->db->update('users');

		return $this->db->affected_rows();
	}
}
?>
