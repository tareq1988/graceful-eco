<?php
class Report extends CI_Model
{
    function get_all()
    {
        return $this->db->get('reports');
    }

    function get($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('reports');

        return $query;
    }
}