<div class="alert alert-success">
    <a class="close" data-dismiss="alert">&times;</a>
    <strong>Success!</strong> You are successfully registered. An E-mail has been sent to your address.

    <p>Please click that link to activate your account</p>
</div>