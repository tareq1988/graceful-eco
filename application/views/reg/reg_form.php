
    <div class="page-header">
        <h1>Registration</h1>
    </div>

        <?php echo validation_errors('<div class="alert alert-error"><a class="close" data-dismiss="alert">&times;</a>', '</div>'); ?>

        <?php
        $email_exists = $this->session->flashdata('email_exists');
		$username_exists = $this->session->flashdata('username_exists');
		
        if($email_exists) echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">&times;</a>' . $email_exists . '</div>';
		if($username_exists) echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">&times;</a>' . $username_exists . '</div>';
        ?>
        <?php echo form_open('account/register', array('class' => 'form-horizontal')) ?>

        <div class="control-group">
            <label class="control-label" for="name">Account Type</label>

            <div class="controls">
                <label><input type="radio" name="type" value="NGO" checked="checked"> NGO</label>
                <label><input type="radio" name="type" value="BUYER"> Company Owner</label>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="name">Name</label>

            <div class="controls">
                <input type="text" id="name" name="name" value="<?php echo set_value('name', '') ?>" size="20" />
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="email">E-Mail</label>

            <div class="controls">
                <input type="text" id="email" name="email" value="<?php echo set_value('email', '') ?>" size="20" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="pw">Password</label>
            
            <div class="controls">
                <input type="password" id="pwd" name="password" size="20" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="con_pw">Confirm Password</label>

            <div class="controls">
                <input type="password" id="c_pwd" name="c_password" size="20" />
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="reg_submit" class="btn btn-primary" value="Register">
			<?php echo anchor('account/login', 'Login') ?>
        </div>
        <?php echo form_close() ?>