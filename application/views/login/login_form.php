
    <div class="page-header">
        <h1>Login</h1>
    </div>

        <?php
			$logged_out = $this->session->flashdata('logged_out');
			$invalid_login = $this->session->flashdata('invalid_login');
			$activated = $this->session->flashdata('activated');

			if( $activated ) echo "<div class='success'>$activated</div>";
        ?>

        <?php if ( $logged_out ) { ?>
            <div class="alert alert-success">
                <a class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo $logged_out; ?>
            </div>
        <?php } ?>

        <?php if ( $invalid_login ) { ?>
            <div class="alert alert-error">
                <a class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?php echo $invalid_login; ?>
            </div>
        <?php } ?>

        <?php echo validation_errors('<div class="error">', '</div>'); ?>
        <?php echo form_open('account/login', array('class' => 'form-horizontal')) ?>

        <div class="control-group">
            <label class="control-label" for="name">E-Mail</label>

            <div class="controls">
                <input type="text" id="email" name="email" value="<?php echo set_value('email', '') ?>" size="20" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="name">Password</label>

            <div class="controls">
                <input type="password" id="pwd" name="password" value="" size="20" />
            </div>
        </div>

        <div class="form-actions">
            <input type="submit" name="submit" class="btn btn-primary" value="Login">
			<?php echo anchor('account/register', 'Signup') ?>
			<?php //echo anchor('account/reset', 'Forgot Password') ?>
        </div>
        <?php echo form_close() ?>
