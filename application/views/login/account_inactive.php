<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Inactive Account</h1>
        <p>account is inactive, please activate the account</p>
        <p>If you didn't receive the activation mail, then enter your email address to resend the activation email</p>

        <?php echo validation_errors(); ?>
        <?php
        $invalid_email = $this->session->flashdata('invalid_email');
        if($invalid_email) echo $invalid_email;

		$activation_resend = $this->session->flashdata('activation_resend');
        if($activation_resend) echo $activation_resend;
        ?>
        <?php echo form_open('account/resend_activation') ?>
        <p>
            <label for="email">E-Mail</label>
            <input type="text" name="email" size="20" value="" />
        </p>
        <p>
            <input type="submit" name="submit" value="Re-send" />
        </p>
        <?php echo form_close() ?>
    </body>
</html>
