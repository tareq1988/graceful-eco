

<ul class="no-list clear-fix section-list">


<!-- Main -->
<li class="text-center clear-fix">
    <?php
    $location = array();

    foreach ($reports->result() as $row)
    {
        $link = anchor('/reports/details/' . $row->id, 'View Details' );
        $location[] = array(
            'latitude' => $row->latitude,
            'longitude' => $row->longitude,
            'html' => "<h3>$row->name</h3><p>$row->description</p> $link"
        );
    }
    ?>
    <div class="span11 margin-top-20 margin-bottom-50 clear-fix">
        <div id="map"></div>
    </div>

    <script type="text/javascript">
        jQuery(function($) {
            $("#map").goMap({
                latitude: 23.709921,
                longitude: 90.407143,
                zoom: 7,
                maptype: 'ROADMAP',
                markers: <?php echo json_encode( $location ); ?>
            });
        });
    </script>

    <h1 class="margin-bottom-0">Effluent Matters</h1>

    <p class="subtitle-paragraph margin-top-20 margin-bottom-50 clear-fix">
        Citizen Mapping of Garment Manufacturer Effluent Outflows.
        <span class="bold">Lets reduce it and save our homeland</span>
    </p>

    <a href="<?php echo(site_url('welcome/report')); ?>" class="button-black clear-fix">
        <span class="click-here"></span>
        Submit Report
    </a>
    <a href="<?php echo(site_url('reports')); ?>" class="button-black">Try For Free</a>

</li>
<!-- /Main -->


<!-- Gallery -->
<li class="padding-0 no-background position-relative">

    <div class="main overflow-hidden">

        <!-- Image list -->
        <ul class="no-list image-list image-list-carousel">

            <!-- Image -->
            <li>
                <h3>Find</h3>
                <div>
                    <a href="<?php echo( base_url());?>asset/images/small/s01.jpg" class="preloader overlay-image fancybox-image" rel="gallery-1">
                        <img src="<?php echo( base_url());?>asset/images/small/s01.jpg" alt="" />
                        <span></span>
                    </a>
                    <p>Find outflows nearby you</p>
                </div>
            </li>
            <!-- /Image -->

            <!-- Image -->
            <li>
                <h3>Capture</h3>
                <div>
                    <a href="<?php echo( base_url());?>asset/images/small/s04.jpg" class="preloader overlay-image fancybox-image" rel="gallery-1">
                        <img src="<?php echo( base_url());?>asset/images/small/s04.jpg" alt="" />
                        <span></span>
                    </a>
                    <p>Capture the image or video</p>
                </div>
            </li>
            <!-- /Image -->

            <!-- Image -->
            <li>
                <h3>Submit</h3>
                <div>
                    <a href="<?php echo( base_url());?>asset/images/small/s03.jpg" class="preloader overlay-image fancybox-image" rel="gallery-1">
                        <img src="<?php echo( base_url());?>asset/images/small/s03.jpg" alt="" />
                        <span></span>
                    </a>
                    <p>Submit the report to us</p>
                </div>
            </li>
            <!-- /Image -->

            <!-- Image -->
            <li>
                <h3>Share</h3>
                <div>
                    <a href="<?php echo( base_url());?>asset/images/small/s05.jpg" class="preloader overlay-image fancybox-image" rel="gallery-1">
                        <img src="<?php echo( base_url());?>asset/images/small/s05.jpg" alt="" />
                        <span></span>
                    </a>
                    <p>Share to others in your community</p>
                </div>
            </li>
            <!-- /Image -->

            <!-- Video -->
            <li>
                <h3>Aware</h3>
                <div>
                    <a href="https://www.youtube.com/embed/Zln9I9IttLA" class="preloader overlay-video fancybox-video-youtube">
                        <img src="<?php echo( base_url());?>asset/images/small/s07.jpg" alt="" />
                        <span></span>
                    </a>
                    <p>Aware your community</p>
                </div>
            </li>
            <!-- /Video -->

            <!-- Video -->
            <li>
                <h3>Keep alive</h3>
                <div>
                    <a href="https://www.youtube.com/embed/6v2L2UGZJAM" class="preloader overlay-video fancybox-video-youtube">
                        <img src="<?php echo( base_url());?>asset/images/small/s06.jpg" alt="" />
                        <span></span>
                    </a>
                    <p>Beautify your city</p>
                </div>
            </li>
            <!-- /Video -->

        </ul>
        <!-- /Image list -->

        <!-- Navigation -->
        <a href="#" class="image-list-carousel-navigation-prev"></a>
        <a href="#" class="image-list-carousel-navigation-next"></a>
        <!-- /Navigation -->

    </div>

</li>
<!-- /Gallery -->


<!-- Features -->
<li>

    <h2>Features</h2>

    <p class="subtitle-paragraph">
        Enables citizens, pourashavas, garment buyers to monitor the-
        <span class="bold">the quantity and quality of effluent outfall.</span>
    </p>

    <div class="layout-50 clear-fix">

        <!-- Left column -->
        <div class="layout-50-left clear-fix">

            <!-- Features list -->
            <ul class="no-list list-2 clear-fix">

                <!-- Item -->
                <li>
                    <h3>Find</h3>
                    <p class="con-1 con-1-1">
                        Find outflows nearby you.
                    </p>
                </li>
                <!-- /Item -->

                <!-- Item -->
                <li>
                    <h3>Capture</h3>
                    <p class="con-1 con-1-2">
                        Capture the image or video
                    </p>
                </li>
                <!-- /Item -->

                <!-- Item -->
                <li>
                    <h3>Submit</h3>
                    <p class="con-1 con-1-3">
                        Submit the report to us.
                    </p>
                </li>
                <!-- /Item -->

                <!-- Item -->
                <li>
                    <h3>Share</h3>
                    <p class="con-1 con-1-4">
                        Share to others in your community.
                    </p>
                </li>
                <!-- /Item -->

                <!-- Item -->
                <li>
                    <h3>Aware</h3>
                    <p class="con-1 con-1-4">
                        Aware your community.
                    </p>
                </li>
                <!-- /Item -->

            </ul>
            <!-- /Features list -->

        </div>
        <!-- /Left column -->

        <!-- Right column -->
        <div class="layout-50-right">

            <!-- Accordion -->
            <div class="nostalgia-accordion clear-fix">

                <h3><a href="#">How does it work</a></h3>
                <div>

                    <div>

                        <ul class="no-list list-1 clear-fix">

                            <li>
                                <span>1</span>
                                <h5>Citizens find outfall</h5>
                                <p>General citizens find the outfalls over their location.</p>
                            </li>
                            <li>
                                <span>2</span>
                                <h5>Capture the picture or video</h5>
                                <p>They cature the picture or video</p>
                            </li>
                            <li>
                                <span>3</span>
                                <h5>Upload via website</h5>
                                <p>They upload via website.</p>
                            </li>
                            <li>
                                <span>4</span>
                                <h5>Send SMS</h5>
                                <p>General citizen send SMS to the defined number.</p>
                            </li>
                            <li>
                                <span>4</span>
                                <h5>Website shows the report</h5>
                                <p>The website shows the report in map.</p>
                            </li>
                        </ul>

                    </div>

                </div>

                <h3><a href="#">App version</a></h3>
                <div>

                    <div>

                        <ul class="no-list list-1 clear-fix">

                            <li>
                                <span class="con-apple"></span>
                                <h5>Apple OS4</h5>
                                <p>Aliquam malesuada arcu eu tortor consequat rhoncuse. Maecenas faucibus sagittis turpis. Praesent scelerisque.</p>
                            </li>
                            <li>
                                <span class="con-apple"></span>
                                <h5>Apple OS5</h5>
                                <p>Proin pretium, lectus sit amet lacinia convallis, elit nunc vehicula eros, quis faucibus risus elit non justo. Donec.</p>
                            </li>
                            <li>
                                <span class="con-android"></span>
                                <h5>Android 2.3.5</h5>
                                <p>Aenean eu orci arcu, sed cursus orci. Cum sociis natoq penatibus et magnis dis parturient montes, nascetur mu.</p>
                            </li>
                            <li>
                                <span class="con-android"></span>
                                <h5>Android 4.0.5</h5>
                                <p>Cras tristique, nisl a placerat dignissim, nibh quam conv metus, sed placerat velit felis quis lacus. In hac dictumst.</p>
                            </li>

                        </ul>

                    </div>

                </div>

                <h3><a href="#">Youtube preview</a></h3>
                <div>

                    <div>

                        <a href="http://www.youtube.com/embed/r-aer2QFybc" class="preloader overlay-video fancybox-video-youtube video-preview">
                            <img src="<?php echo( base_url());?>asset/images/img3.jpg" alt="" />
                            <span></span>
                        </a>

                    </div>

                </div>

                <h3><a href="#">Vimeo preview</a></h3>
                <div>

                    <div>

                        <a href="http://player.vimeo.com/video/10470386" class="preloader overlay-video fancybox-video-vimeo video-preview">
                            <img src="<?php echo( base_url());?>asset/images/img1.jpg" alt="" />
                            <span></span>
                        </a>

                    </div>

                </div>

            </div>
            <!-- /Accordion -->

        </div>
        <!-- /Right column -->

    </div>

</li>
<!-- /Features -->


<!-- Testimonials -->
<li>

    <h2>Testimonials</h2>

    <p class="subtitle-paragraph">
        Quisque sodales metus et mauris imperdiet non volutpat.
        <span class="bold">Nulla turpis velit, imperdiet vel viverra ut, tristique sit amet felis.</span>
    </p>

    <ul class="no-list clear-fix testimonial-list layout-50">

        <!-- Item -->
        <li class="layout-50-left">
            <p>Curabitur vehicula mi in quam molestie a eleifend magna phare. Mauris mattis faucibus porttitor mauris molestie orci neque.</p>
            <div>
                <span class="bold testimonial-list-arrow">Superb!</span>
									<span class="testimonial-list-author">
										<span class="bold">John Doe</span>, Founder World Company
									</span>
            </div>
        </li>
        <!-- /Item -->

        <!-- Item -->
        <li class="layout-50-right">
            <p>Vivamus posuere dapibus elit, id rutrum mauris scelerisque et. Curabitur augue erat, laoreet ut pharetra vel, convallis quis lor.</p>
            <div>
                <span class="bold testimonial-list-arrow">Superb!</span>
									<span class="testimonial-list-author">
										<span class="bold">Anna Brown</span>, Big Company
									</span>
            </div>
        </li>
        <!-- /Item -->

        <!-- Item -->
        <li class="layout-50-left">
            <p>Ut luctus tortor nulla, sed commodo lorem. Donec lacus eros, lobortis ac faucibus et, cursus sed tortor. Nunc at lacus id diam.</p>
            <div>
                <span class="bold testimonial-list-arrow">Superb!</span>
									<span class="testimonial-list-author">
										<span class="bold">Mark Green</span>, Smooth Communications
									</span>
            </div>
        </li>
        <!-- /Item -->

        <!-- Item -->
        <li class="layout-50-right">
            <p>Suspendisse egestas dui non lorem interdum cursus. Ut justo condimentum ligula sed eros consectetur rhoncus sed nec eros.</p>
            <div>
                <span class="bold testimonial-list-arrow">Superb!</span>
									<span class="testimonial-list-author">
										<span class="bold">Ursula White</span>, Suites &amp; Communication Ltd.
									</span>
            </div>
        </li>
        <!-- /Item -->

    </ul>

</li>
<!-- /Testimonials -->


<!-- Contact -->
<li>

    <h2>Contact Us</h2>

    <p class="subtitle-paragraph">
        Mauris a neque pulvinar orci tempor rutrum at vitae massa.
        <span class="bold">Integer accumsan erat aliquam mauris placerat ullamcorper donec sed dui.</span>
    </p>

    <div class="clear-fix layout-50">

        <!-- Left column -->
        <div class="layout-50-left">

            <h3>On The Map</h3>

            <div class="contact-details-wrapper">

                <!-- Google Maps -->
                <div id="map_bottom"></div>
                <!-- /Google Maps -->

                <!-- Contact details -->
                <div class="contact-details">

                    <!-- Contact details button -->
                    <a href="#" id="contact-details-button"></a>
                    <!-- /Contact details button -->

                    <div class="clear-fix layout-50p">

                        <div class="layout-50p-left">

                            Nostalgia<br/>
                            Quai Henri IV<br/>
                            75004 Paris, France

                        </div>

                        <div class="layout-50p-right">

                            <ul class="no-list">
                                <li class="con-2 con-2-1">T: 877 123 0223</li>
                                <li class="con-2 con-2-2">F: 877 123 0224</li>
                                <li class="con-2 con-2-3">E: nostalgia@mail.com</li>
                            </ul>

                        </div>

                    </div>

                </div>
                <!-- /Contact details -->

            </div>

        </div>
        <!-- /Left column -->

        <!-- Right column -->
        <div class="layout-50-right">

            <h3>General Inquiry</h3>

            <!-- Contact form -->
            <form name="contact-form" id="contact-form" action="" method="post" class="clear-fix">

                <div class="clear-fix">

                    <ul class="no-list form-line">

                        <li class="clear-fix block">
                            <label for="contact-form-name">Your name</label>
                            <input type="text" name="contact-form-name" id="contact-form-name" value=""/>
                        </li>
                        <li class="clear-fix block">
                            <label for="contact-form-mail">Your e-mail</label>
                            <input type="text" name="contact-form-mail" id="contact-form-mail" value=""/>
                        </li>
                        <li class="clear-fix block">
                            <label for="contact-form-message">Your message</label>
                            <textarea name="contact-form-message" id="contact-form-message" rows="1" cols="1"></textarea>
                        </li>
                        <li class="clear-fix block">
                            <input type="submit" id="contact-form-send" name="contact-form-send" class="button" value="Send"/>
                        </li>

                    </ul>

                </div>

            </form>
            <!-- /Contact form -->

        </div>
        <!-- /Right column -->

    </div>

</li>
<!-- Contact -->

</ul>