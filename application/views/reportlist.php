<?php
//var_dump($reports)

foreach ($reports->result() as $row) {
    ?>

<div class="item">
    <div class="span2">
    	<?php if( $row->image ) { ?>
	    	<?php $url = base_url() . 'uploads/images/' . $row->image; ?>
	    	<a href="<?php echo $url; ?> ?>"><img class="thumbnail" width="75" src="<?php echo $url; ?>" alt=""></a>
	    <?php } else { ?>
	    	<img src="http://dummyimage.com/75x75/b8b1b8/fff.png&text=No+Image" alt="no image" class="thumbnail">
	    <?php } ?>
    </div>

    <div class="span4">
    	<a href="<?php echo site_url('/reports/details/' . $row->id ); ?>"><?php echo $row->description; ?></a>
    </div>

    <div class="span3">
    	<?php echo $row->location; ?>
    </div>

    <div class="span1">
    	<?php echo $row->severity; ?>
    </div>

    <div class="span2">
    	<?php echo date( 'j F, Y', strtotime($row->time) ); ?>

        <?php if ( $this->auth->is_logged_in() && $this->auth->get_role() == 'NGO' && !$row->verified ) { ?>
            <a class="btn btn-success" href="<?php echo site_url('/reports/approve/' . $row->id); ?>">Approve</a>
        <?php } ?>

        <?php if( $row->verified ) { ?>
            <span class="label label-info">verified</span>
        <?php } ?>
    </div>

</div>
<?php
}
?>