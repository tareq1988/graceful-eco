    <footer class="footer span12">
        <p>&copy; Company 2012</p>
    </footer>

    </div><!-- .row -->
</div><!-- .container -->

<?php
if( count($footer_js) )
{
    foreach($footer_js as $js)
    {
        ?>
        <script type="text/javascript" src="<?php echo($js); ?>"></script>
        <?php
    }
}
?>
</body>
</html>
