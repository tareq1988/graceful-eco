
</div>
<!-- /Content -->


<!-- Footer -->
<div class="footer layout-50 clear-fix">

    <!-- Left column -->
    <div class="layout-50-left">

        <h3>Network With Us</h3>

        <!-- Latest tweets -->
        <div id="latest-tweets"></div>
        <!-- /Latest tweets -->

        <!-- Social icons list -->
        <ul class="no-list social-list clear-fix">
            <li><a href="#" class="social-list-twitter"></a></li>
            <li><a href="#" class="social-list-facebook"></a></li>
            <li><a href="#" class="social-list-googleplus"></a></li>
        </ul>
        <!-- /Social icons list -->

    </div>
    <!-- /Left column -->

    <!-- Right column -->
    <div class="layout-50-right">

        <h3>Join Our Mailing List</h3>

        <p>
            Get the latest news and updates with the Nostalgia newsletter.<br/>
            Join our monthly email newsletter. It's fast and easy.
        </p>

        <!-- Newsletter form -->
        <form name="newsletter-form" id="newsletter-form" action="" method="post" class="clear-fix">

            <div class="clear-fix">

                <ul class="no-list form-line">

                    <li class="clear-fix block">
                        <label for="newsletter-form-mail">Your e-mail</label>
                        <input type="text" name="newsletter-form-mail" id="newsletter-form-mail" value=""/>
                        <input type="submit" id="newsletter-form-send" name="newsletter-form-send" class="button" value="Send"/>
                    </li>

                </ul>

            </div>

        </form>
        <!-- /Newsletter form -->

    </div>
    <!-- /Right column -->

</div>
<!-- /Footer -->

</div>

<!-- Background overlay -->
<div id="background-overlay"></div>
<!-- /Background overlay -->
<?php
if( count($footer_js) )
{
    foreach($footer_js as $js)
    {
        ?>
    <script type="text/javascript" src="<?php echo($js); ?>"></script>
    <?php
    }
}
?>
</body>

</html>