<?php $active_menu = isset( $menu ) ? $menu : 'home'; ?>
<!DOCTYPE html>
<html lang="en" class="fuelux">
<head>
    <meta charset="utf-8">
    <title><?php echo( (isset( $page_title ) ? $page_title . ' | ' : '') . $this->config->item('app_title') ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo($this->config->item('description')); ?>">
    <meta name="author" content="<?php echo($this->config->item('author')); ?>">

    <?php
    if( count($header_css) )
    {
        echo('<!-- Le styles -->');
        foreach($header_css as $css)
        {
            ?>
            <link href="<?php echo($css); ?>" rel="stylesheet" type="text/css" />
            <?php
        }
    }

    if( count($header_js) )
    {
        echo('<!-- Le js -->');
        foreach($header_js as $js)
        {
            ?>
            <script type="text/javascript" src="<?php echo($js); ?>"></script>
            <?php
        }
    }
    ?>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="con-bar"></span>
                <span class="con-bar"></span>
                <span class="con-bar"></span>
            </a>
            <a class="brand" href="<?php echo site_url('en'); ?>">Graceful Eco</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li <?php if( $active_menu == 'home' ) echo 'class="active"'; ?>><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li <?php if( $active_menu == 'new_report' ) echo 'class="active"'; ?>><a href="<?php echo site_url(array('welcome', 'report')); ?>">Add Report</a></li>
                    <li <?php if( $active_menu == 'report_list' ) echo 'class="active"'; ?>><a href="<?php echo site_url(array('reports', 'index')); ?>">Report List</a></li>

                    <?php if ( $this->auth->is_logged_in() ) { ?>
                        <li><a href="<?php echo site_url(array('account', 'logout')); ?>">Logout</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo site_url(array('account', 'login')); ?>">Login</a></li>
                    <?php } ?>
                    
                </ul>
                <!--
                <form class="navbar-form pull-right">
                  <input class="span2" type="text" placeholder="Email">
                  <input class="span2" type="password" placeholder="Password">
                  <button type="submit" class="btn">Sign in</button>
                </form>
              -->
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>

<div class="container wrapper">
    <div class="row">