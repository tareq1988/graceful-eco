<?php
$location = array();

foreach ($reports->result() as $row)
{
    $link = anchor('/reports/details/' . $row->id, 'View Details' );
    $location[] = array(
                    'latitude' => $row->latitude,
                    'longitude' => $row->longitude,
                    //'html' => "<h3>$row->name</h3><p>$row->description</p> $link"
                    'html' => "<h3>" . $row->location . "</h3>" . $link,
                );
}
?>
<div class="span12">
    <div id="map"></div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $("#map").goMap({
        latitude: 23.709921,
        longitude: 90.407143,
        zoom: 7,
        maptype: 'ROADMAP',
        markers: <?php echo json_encode( $location ); ?>
    });
});
</script>