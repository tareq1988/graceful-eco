<?php $detail =  $details->result(); ?>
<div class="span4">
    <?php if( $detail[0]->image ) { ?>
        <?php $url = base_url() . 'uploads/images/' . $detail[0]->image; ?>
        <a href="<?php echo $url; ?> ?>"><img class="thumbnail" src="<?php echo $url; ?>" alt=""></a>
    <?php } else { ?>
        <img src="http://dummyimage.com/300x300/b8b1b8/fff.png&text=No+Image" alt="no image" class="thumbnail">
    <?php } ?>
</div>

<div class="span8">
    <h2><?php echo $detail[0]->location; ?></h2>


    <strong>Details:</strong>
    
    <?php if( $detail[0]->verified ) { ?>
        <span class="label label-info">verified</span>
    <?php } else { ?>
        <span class="label label-important">un-verified</span>
    <?php } ?>

    <p><?php echo $detail[0]->description; ?></p>

    <strong>Location:</strong>
    <p>
        <div id="map" style="width: 300px; height: 300px;"></div>
    </p>

    <p><strong>Severity:</strong> <?php echo $detail[0]->severity; ?></p>
</div>

<script type="text/javascript">
    jQuery(function() {
        $("#map").goMap({
            zoom: 7,
            maptype: 'ROADMAP',
            markers: [
                {
                    latitude: <?php echo $detail[0]->latitude; ?>,
                    longitude: 90.361481,
                }
            ]
        });
    });
</script>