<div class="span12">
	<?php if ($successful): ?>
	<div class="alert alert-success">
		<a class="close" data-dismiss="alert">&times;</a>
		<strong>Success!</strong> You successfully read this important alert message.
	</div>
	<?php endif; ?>

	<?php echo form_open_multipart('', array('class' => 'form-horizontal', 'id' => 'add-report')); ?>
	<fieldset>
		<input type="hidden" name="api_key" value="Hello-Api">
		<legend>Add New Report</legend>
		<div class="control-group">
			<label class="control-label" for="name">Name</label>

			<div class="controls">
				<input type="text" class="required input-xlarge" name="name" id="name" placeholder="Enter your name" value="<?php echo set_value('name') ?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="email">E-Mail</label>

			<div class="controls">
				<input type="text" class="input-xlarge required email" name="email" id="email" placeholder="Enter your email address" value="<?php echo set_value('email') ?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="address">Address</label>

			<div class="controls">
				<input type="text" class="input-xlarge" name="latlang" id="locationpicker" placeholder="Where are you from" value="<?php echo set_value('latlang') ?>">
				<input type="hidden" name="address" id="address" value="<?php echo set_value('address') ?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="textarea">Details</label>

			<div class="controls">
				<textarea class="input-xlarge" id="textarea" rows="3" name="details" placeholder="Describe the problem"><?php echo set_value('details') ?></textarea>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="textarea">Nearest Plants</label>

			<div class="controls">
				<input type="text" name="plants" data-provide="typeahead" data-source='["Gazipur", "Sylhet"]' value="<?php echo set_value('plants') ?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="image">Photo</label>

			<div class="controls">
				<input class="input-file" id="image" name="image" type="file">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="video">Video</label>

			<div class="controls">
				<input class="input-file" id="video" name="video" type="file">
			</div>
		</div>

		<legend>Technical Data</legend>

		<div class="control-group">
			<label class="control-label" for="input01">pH Level</label>

			<div class="controls">
				<input type="hidden" name="tech_name[]" value="ph">
				<input type="text" class="input-xlarge" name="tech_value[]" id="input01" placeholder="pH level of water" value="<?php echo set_value("tech_value[0]")?>">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="input01">Chromium Level</label>

			<div class="controls">
				<input type="hidden" name="tech_name[]" value="chromium">
				<input type="text" class="input-xlarge" name="tech_value[]" id="input01" placeholder="Chromium level of water" value="<?php echo set_value("tech_value[1]")?>">
			</div>
		</div>

		<div class="form-actions">
			<button type="submit" name="add_report" class="btn btn-primary">Save changes</button>
		</div>
	</fieldset>
	</form>
</div>