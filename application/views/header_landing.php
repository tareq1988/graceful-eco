<!DOCTYPE html>
<html lang="en" class="fuelux">
<head>
    <meta charset="utf-8">
    <title><?php echo( (isset( $page_title ) ? $page_title . ' | ' : '') . $this->config->item('app_title') ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo($this->config->item('description')); ?>" />
    <meta name="author" content="<?php echo($this->config->item('author')); ?>" />

    <link rel="stylesheet" type="text/css" media="screen and (max-width:969px)" href="<?php echo( base_url().'asset/css/'); ?>width-0-969.css"/>
    <link rel="stylesheet" type="text/css" media="screen and (max-width:767px)" href="<?php echo( base_url().'asset/css/'); ?>width-0-767.css"/>

    <link rel="stylesheet" type="text/css" media="screen and (min-width:480px) and (max-width:969px)" href="<?php echo( base_url().'asset/css/'); ?>width-480-969.css"/>
    <link rel="stylesheet" type="text/css" media="screen and (min-width:768px) and (max-width:969px)" href="<?php echo( base_url().'asset/css/'); ?>width-768-969.css"/>
    <link rel="stylesheet" type="text/css" media="screen and (min-width:480px) and (max-width:767px)" href="<?php echo( base_url().'asset/css/'); ?>width-480-767.css"/>
    <link rel="stylesheet" type="text/css" media="screen and (max-width:479px)" href="<?php echo( base_url().'asset/css/'); ?>width-0-479.css"/>

    <?php
    if( count($header_css) )
    {
        echo('<!-- Le styles -->');
        foreach($header_css as $css)
        {
            ?>
            <link href="<?php echo($css); ?>" rel="stylesheet" type="text/css" />
            <?php
        }
    }

    if( count($header_js) )
    {
        echo('<!-- Le js -->');
        foreach($header_js as $js)
        {
            ?>
            <script type="text/javascript" src="<?php echo($js); ?>"></script>
            <?php
        }
    }
    ?>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>

<div class="main main-body">

    <!-- Header -->
    <div class="header clear-fix">

        <div class="layout-50p clear-fix">

            <!-- Logo -->
            <div class="layout-50p-left clear-fix">
                <a href="#" class="header-logo"><?php echo($this->config->item('app_title')); ?></a>
            </div>
            <!-- /Logo -->

            <div class="layout-50p-right">

                <div class="header-phone">
                    Send SMS to 9988
                </div>

            </div>

        </div>

    </div>
    <!-- /Header -->

    <!-- Content -->
    <div class="content clear-fix">