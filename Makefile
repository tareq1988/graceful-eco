SITE_ROOT=/srv/sites/graceful-eco

all: deploy

deploy:
	-rsync -avr ./ --exclude=".git" --exclude=license.txt --exclude=Makefile --exclude=extra --exclude=user_guide $(SITE_ROOT)/

clean:
	-rm -rf $(SITE_ROOT)/application
	-rm -rf $(SITE_ROOT)/asset
	-rm -rf $(SITE_ROOT)/index.php
	-rm -rf $(SITE_ROOT)/.htaccess
