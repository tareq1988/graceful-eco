CREATE TABLE `users` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `name` varchar(150) NOT NULL,
 `password` varchar(40) NOT NULL,
 `email` varchar(40) NOT NULL,
 `type` enum('BUYER','NGO','ADMIN') NOT NULL,
 `registered` datetime NOT NULL,
 `last_login` datetime NOT NULL,
 `active` int(1) NOT NULL,
 `activation_code` varchar(40) NOT NULL DEFAULT '0',
 `forgotten_password_code` varchar(40) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`),
 UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from` int(11) unsigned  NOT NULL,
  `to` int(11) unsigned  NOT NULL,
  `read` tinyint(1) DEFAULT '0',
  `message` varchar(400) DEFAULT NULL,
  FOREIGN KEY (`from`)  REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`to`)  REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `location` varchar(30) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `video` varchar(250) DEFAULT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  `severity` tinyint(4) DEFAULT 0,
  `verified` tinyint(1) DEFAULT 0,
  `source` varchar(255) NOT NULL COMMENT 'Based on source_type it can be a phone number or email',
  `source_type` enum('WEB', 'API', 'SMS') NOT NULL,
  `source_name` varchar(255) DEFAULT NULL COMMENT 'name of the man who submiteed the report' , 
  `description` varchar(1000) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  PRIMARY KEY (`time`, `source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `plants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `location` varchar(30) NOT NULL,
  `latitude` decimal(10,6) DEFAULT NULL,
  `longitude` decimal(10,6) DEFAULT NULL,
  UNIQUE KEY (`id`),
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `technicals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `value` varchar(255) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`report_id`,`name`),
  UNIQUE KEY `id` (`id`),
  KEY `name` (`name`),
  CONSTRAINT `technicals_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

